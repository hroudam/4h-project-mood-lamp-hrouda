# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | +-6 hodin                       |
| odkud jsem čerpal inspiraci                   | https://www.instructables.com/RGB-Lamp-WiFi/ |
| odkaz na video                                | https://www.youtube.com/watch?v=bLu3WkV305g&ab_channel=misa816Gaming                    |
| jak se mi to podařilo rozplánovat             | příprava                        |
| proč jsem zvolil tento design                 | ztratil jsem z minulého víko, jednoduchý a elegantní |
| zapojení                                      | ![My Image](/dokumentace/schema/návrh_mood_lamp.jpg)          |
| z jakých součástí se zapojení skládá          | LED páska, měřič teploty a vlhkosti, WEMOS   |
| realizace                                     | ![My Image2](/dokumentace/fotky/mood_lamp.jpeg) |
| UI                                            | ![My Image3](/dokumentace/fotky/UI.PNG)                 |
| co se mi povedlo                              | zapojení, design                |
| co se mi nepovedlo/příště bych udělal/a jinak | místo papírového krytu bych dal plastový     |
| zhodnocení celé tvorby (návrh známky)         | 1                               |