#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>
#include <cstdlib>
#include "DHT.h"

//***ASYNC
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
AsyncWebServer server(80);
String ipadress;
//***

//----------------------------------------------------------------------------------------
#define pinDIN D6
#define pocetLED 140
Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(pocetLED, pinDIN, NEO_GRB + NEO_KHZ800);
int currentMode = -1;
//---
#define pinDHT D7
#define typDHT11 DHT11            
DHT mojeDHT(pinDHT, typDHT11);
//----------------------------------------------------------------------------------------

const char* ssid = "Hrouda_puda";
const char* password = "0605292461";
const char* mqtt_server = "broker.hivemq.com";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

void setup_wifi() {

  delay(10);
  
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  //***
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Hi! This is a sample response.");
  });

  AsyncElegantOTA.begin(&server);    // Start AsyncElegantOTA
  server.begin();
  Serial.println("HTTP server started");
  //***
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if(strcmp(topic,"hroudam/moodlamp/strip/rgb")==0){
    currentMode = -1;
    setStripRGB(payload, length);
  }

  if(strcmp(topic,"hroudam/moodlamp/strip/modes")==0){    
    if((char)payload[0] == '0'){    //OFF
      currentMode = -1;
      setStripMode0();
    }
    if((char)payload[0] == '1'){    //RAINBOW
      currentMode = -1;
      setStripMode1();
    }
  }
}

void reconnect() {
  
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.publish("hroudam/moodlamp/test", "Zapnuto");
      client.subscribe("hroudam/moodlamp/strip/modes");
      client.subscribe("hroudam/moodlamp/strip/rgb");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

//------------------------------------------------------------------------

void setup() {
  //---------------
  rgbWS.begin();
  //---
  mojeDHT.begin();
  //---------------
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  //---
  startMoodLamp();
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  //---
  unsigned long now2 = millis();
  if(now2 - lastMsg > 2000){
    lastMsg = now2;
    readTemperatureAndHumidity();
  }
  //---

  unsigned long now = millis();
  if (now - lastMsg > 10000) {
    lastMsg = now;
    ++value;
    snprintf (msg, MSG_BUFFER_SIZE, "Automatická kontrolní zpráva #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("hroudam/moodlamp/test", msg);
  }

  if (currentMode == 1){
    setStripMode1();
  }
}

//------------------------------------------------------------------------

void nastavRGB (byte r, byte g, byte b, int cislo) {
  uint32_t barva;
  barva = rgbWS.Color(r, g, b);
  rgbWS.setPixelColor(cislo - 1, barva);
  rgbWS.show();
}

void startMoodLamp(){
  for(int j=0; j<3; j++){
    for(int i=1; i<=60; i++){
      nastavRGB(255,0,0,i);
    }
    delay(100);
    for(int i=1; i<=60; i++){
      nastavRGB(0,0,0,i);
    }
    delay(100);
  }

  
}

void setStripRGB(byte* payload, unsigned int length){                       //RGB
  payload[length] = '\0'; // Přidání ukončovací nuly pro vytvoření řetězce
    String payloadStr = String((char*)payload);

    // Hledání hodnot r, g, b ve formátu "rgb(x, y, z)"
    int r, g, b;
    if (sscanf(payloadStr.c_str(), "rgb(%d, %d, %d)", &r, &g, &b) == 3) {
      Serial.print("r = ");
      Serial.print(r);
      Serial.print(" | g = ");
      Serial.print(g);
      Serial.print(" | b = ");
      Serial.println(b);
      for(int i=1; i<=60; i++){
        nastavRGB(r,g,b,i);
      }
    } else {
      Serial.println("Invalid RGB format");
    }
}

void setStripMode0(){               //OFF
  for(int i=1; i<=60; i++){
      nastavRGB(0,0,0,i);
  }
}

void setStripMode1(){               //RAINBOW
   randomColorEffect();
}

//---

void readTemperatureAndHumidity(){
  float tep = mojeDHT.readTemperature();
  float vlh = mojeDHT.readHumidity();
  if (isnan(tep) || isnan(vlh)) {
    Serial.println("Chyba při čtení z DHT senzoru!");
  } else {
    Serial.print("Teplota: "); 
    Serial.print(tep);
    Serial.print(" stupnu Celsia, ");
    Serial.print("vlhkost: "); 
    Serial.print(vlh);
    Serial.println("  %");

    String tepStr = String(tep, 2); // Převod float na řetězec s dvěma desetinnými místy
    String vlhStr = String(vlh, 2); // Převod float na řetězec s dvěma desetinnými místy
    client.publish("hroudam/moodlamp/temperature", tepStr.c_str());
    client.publish("hroudam/moodlamp/humidity", vlhStr.c_str());

    //***
    ipadress =  WiFi.localIP().toString();
    client.publish("hroudam/moodlamp/ip", ipadress.c_str());
    //***
  }
}

void randomColorEffect() {
  for(int x = 0; x < 50000; x++){
  const unsigned long updateInterval = 500;  // Aktualizovat barvu každých 100 ms
  static unsigned long lastUpdateTime = 0;

  unsigned long currentTime = millis();

  if (currentTime - lastUpdateTime >= updateInterval) {
    lastUpdateTime = currentTime;

    // Generování náhodných hodnot pro R, G, B
    int randomR = random(256);
    int randomG = random(256);
    int randomB = random(256);

    // Nastavení náhodné barvy pro všechny LED diody
      for(int i=1; i<=60; i++){
          nastavRGB(randomR, randomG, randomB, i);
      }
    }
    delay(500);
  }
}
